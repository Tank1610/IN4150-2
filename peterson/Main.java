package peterson;

import java.util.concurrent.CountDownLatch;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.Arrays;

public class Main {
    public static void main(String[] argv) throws InterruptedException
    // the throws InterruptedException is needed for the countdown latch...
    {
        //Number of nodes, beginning id
        int N = 5;
        int offset = 1;
        Thread[] P = new Thread[N];
        int[] id_list = new int[N];
        Random rand = new Random();

        //Randomly assign an integer id for the nodes
        for (int i = 0; i < N; i++)
        {
            int new_id = rand.nextInt(N+offset) + offset;
            boolean id_exists = IntStream.of(id_list).anyMatch(x -> x == new_id);
            if (!id_exists)
            {
                id_list[i] = new_id;
            }
        }
        System.out.println("ID list of the ring network: ");
        System.out.println(Arrays.toString(id_list));
        
        //Start the nodes
        for(int i = 0; i < N; i++)
        {
            if (i < N-1)
            {
                P[i] = new Thread(new Component(id_list[i], id_list[i+1]));
                P[i].start();
            }else{
                P[i] = new Thread(new Component(id_list[i], id_list[0]));
                P[i].start();
            }

        }


    }



}
