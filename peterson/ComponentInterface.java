package peterson;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;


public interface ComponentInterface extends Remote {
    public int receive(int t, int sender_id) throws RemoteException;
}