package peterson;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;


public class Component implements Runnable {

    private int id;

    //Neighbour's id
    private int n_id;

    private static Registry registry;

    public Component(int remote_id, int remote_n_id)
    {
        this.id = remote_id;
        this.n_id = remote_n_id;
    }

    public synchronized void server() {
        
                try {
                    ComponentInterface skeleton = (ComponentInterface) UnicastRemoteObject.exportObject(new ComponentImplementation(id, n_id), 10000 + id);
        
                    registry = LocateRegistry.createRegistry(10000+id);
                    registry.rebind("Receive", skeleton); 
        
                    System.out.println("Server "+ this.id +" started.");
        
                } catch (Exception e) {
                    e.printStackTrace();
                }
        
            }

    public void main_process() {
        try {

            server();

            System.out.println("Starting client "+ this.id +" with neighbour: "+this.n_id);
            Registry reg;
            ComponentInterface stub ;

            reg = LocateRegistry.getRegistry(10000+n_id);
            stub = (ComponentInterface) java.rmi.Naming.lookup("rmi://localhost:" + (10000 + n_id) + "/Receive");

            while(true)
            {
                //send the node's own id to the neighbour
                stub.receive(id, id);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        main_process();
    }
}