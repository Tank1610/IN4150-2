# IN4150-2

Currently, the code spawns threads (nodes) and assigns them random integer ID's. Each node is initialised with its own ID's as well as the ID of its neighbour (the neighbour that it can send a message to). Each node performs one send action to its neighbour.
In the lab document, there are several steps described for doing this and here are some comments I have on them.
+ The first step requires that it must be possible to specify how many nodes are used and their starting ID's. For now I just hardcoded these in.
+ The second step is to implement 'a single round' of the algorithm. My understanding of a single round is when each node sends its ID to its neighbour.
