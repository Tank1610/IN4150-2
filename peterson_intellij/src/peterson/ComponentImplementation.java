package peterson;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class ComponentImplementation implements ComponentInterface {


    private int down_neighbour;
    private int uid, id, NextRoundID, n_id, nn_id, N; // id of the node and its neighbour's id
    private boolean isPassive;
    // Not putting "volatile" make it not run SOMETIMES.
    // The IDE advised me to put it, it seems better. Don't know what it does though.
    private volatile boolean isNIDnew, isNNIDnew;
    private Registry reg;
    private ComponentInterface stub ;

    // Access to synchronization server, to help all nodes to run only one round at a time
    private Registry reg2;
    private StartupSynchroInterface syncstub;

    private boolean shouldBePassive;


    public ComponentImplementation(int remote_id, int selfport_offset, int N) throws RemoteException, MalformedURLException, NotBoundException {

        this.down_neighbour = (selfport_offset + 1)%N;
        this.uid = remote_id;
        this.id = this.uid;
        this.NextRoundID = this.id;
        this.N = N;
        this.isPassive = false;
        this.shouldBePassive = false;
        this.isNIDnew = false;
        this.isNNIDnew = false;

        reg2 = LocateRegistry.getRegistry(20000);
        syncstub = (StartupSynchroInterface) java.rmi.Naming.lookup("rmi://localhost:" + (20000) + "/Synchro");
        syncstub.down();
    }
    public void connect() throws RemoteException, MalformedURLException, NotBoundException {

        // registering neighbour
        reg = LocateRegistry.getRegistry(10000+down_neighbour);
        stub = (ComponentInterface) java.rmi.Naming.lookup("rmi://localhost:" + (10000 + down_neighbour) + "/Receive");
    }

    public void main_proc_nid() throws RemoteException, MalformedURLException, NotBoundException {

        if (!isPassive) {
            System.out.println("Server UID : " + uid + " ,  Virtual ID : " + id + ", local port : " + ((down_neighbour + N - 1) % N) + " , neighbour at port " + down_neighbour);
            stub.receive_nid(id); // sends its own ID.
        }
    }

    public void main_proc_nnid() throws RemoteException, MalformedURLException, NotBoundException {
        if (! isPassive) {
            stub.receive_nnid(Math.max(id, n_id));
        }
    }

    public void main_proc_check() throws RemoteException, MalformedURLException, NotBoundException {
        if (! isPassive)
            if (n_id >= id && n_id >= nn_id) {
                // remains active
                System.out.println("Comparing nid " + n_id + " and " + nn_id + " - Server UID : " + uid + " ,  Virtual ID : " + id + " updated to " + n_id);
                NextRoundID = n_id;
            } else {
                System.out.println("Comparing nid " + n_id + " and " + nn_id + " - Server UID : " + uid + " ,  Virtual ID : " + id + " is now passive");
                shouldBePassive = true;
            }
    }



    public void reassign_id() throws RemoteException, MalformedURLException, NotBoundException {
        System.out.println("VID " + id + " -> "+NextRoundID);
        id = NextRoundID;
        isPassive = shouldBePassive;
    }

    public void receive_nid(int sender_id) throws RemoteException {
        if (sender_id == id && !isPassive)
        {
            System.out.flush();
            System.out.println("================ FINISHED ============== UID : " + uid + " says : Leader elected: "+id);
            System.exit(0);
        }

        if(isPassive)
        {
            System.out.println("[FORWARDER] Server UID : "+uid+" ,  Virtual ID : "+ id +" forwards nid "+sender_id+" to port "+down_neighbour);
            stub.receive_nid(sender_id);
        } else {
            this.n_id = sender_id;
            isNIDnew = true;
        }
    }

    public void receive_nnid(int sender_id) throws RemoteException, MalformedURLException, NotBoundException {
        if(isPassive)
        {
            System.out.println("Server UID : "+uid+" ,  Virtual ID : "+ id +" passive, forwards nnid "+sender_id+" to port "+down_neighbour);            
            stub.receive_nnid(sender_id);
        } else {
        this.nn_id = sender_id;
        isNNIDnew = true;
        }
    }



}