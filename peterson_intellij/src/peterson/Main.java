package peterson;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.Arrays;

public class Main {
    public static void main(String[] argv) throws InterruptedException, RemoteException, MalformedURLException, NotBoundException
    // the throws InterruptedException is needed for the countdown latch...
    {

        //Number of nodes, beginning id
        int N = 5;
        int offset = 10;

        System.setProperty("java.rmi.server.hostname","localhost");


        // I'm using an ArrayList because it's easier to check if something is inside.
        // Your Random assignation wasn't good, it assigned multiple times the same ID.
        ArrayList<Integer> id_list = new ArrayList<Integer>();
        Random rand = new Random();

        //Get arguments and construct list of nodes
        if (argv.length > 1)
        {
            N = argv.length;
            System.out.println("Number of nodes: "+N);
            for (int i = 0; i < argv.length; i++)
            {
                id_list.add(Integer.parseInt(argv[i]));
                System.out.println("Node id: "+argv[i]+" added");
            }
        }else if(argv.length == 1){
            System.out.println("Only one argument given, randomly generating " +argv[0]+" nodes.");
            N = Integer.parseInt(argv[0]);
            //Randomly assign an integer id for the nodes
            for (int i = 0; i < N; i++)
            {
                // trying to give a new number
                int new_id = rand.nextInt(N+offset) + offset;
                // while that number exists in the list, try a new one
                while(id_list.contains(new_id))
                    new_id = rand.nextInt(N+offset) + offset;

                // now the number shouldn't be in the list, so let's add it.
                id_list.add(new_id);
            }
        }

        Thread[] P = new Thread[N];


        System.out.println("ID list of the ring network: " + N + " nodes : ");
        System.out.println(id_list);
        // display the expected result (easier to read)
        int max = id_list.get(0);
        for (int i = 0; i < N; i++)
        {
            if (max < id_list.get(i))
                max = id_list.get(i);
        }
        System.err.println("Maximum (expected leader) : "+max);

        // Creating ang lauching synchro server
        StartupSynchroInterface skeleton = (StartupSynchroInterface) UnicastRemoteObject.exportObject(new StartupSynchro(N), 20000);
        Registry registry;
        registry = LocateRegistry.createRegistry(20000);
        registry.rebind("Synchro", skeleton);

        Thread.sleep(1000);
        
        //Start the nodes
        for(int i = 0; i < N; i++)
        {
                P[i] = new Thread(new Component(id_list.get(i), i, N));
                P[i].start();
        }
    }



}
