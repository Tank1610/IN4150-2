package peterson;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;


public class Component implements Runnable {

    private int id, N;
    private int selfport_offset;

    private static Registry registry;

    public Component(int remote_id, int port, int remote_N)
    {
        this.id = remote_id;
        this.N = remote_N;
        this.selfport_offset = port;
    }

    public synchronized void server() {
        
                try {
                    ComponentInterface skeleton = (ComponentInterface) UnicastRemoteObject.exportObject(new ComponentImplementation(id, selfport_offset, N), 10000 + selfport_offset);
                    registry = LocateRegistry.createRegistry(10000+selfport_offset);
                    registry.rebind("Receive", skeleton);
                    System.out.println("Server "+ this.id +" started.");

                } catch (Exception e) {
                    e.printStackTrace();
                }
        
            }

    public void main_process() {
        try {
            // should include some countdown latch (same as in Assignment 1) to prevent the clients to start before the servers.
            server();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        main_process();
    }
}