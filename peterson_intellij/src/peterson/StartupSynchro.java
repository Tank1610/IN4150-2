package peterson;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class StartupSynchro implements StartupSynchroInterface{

    private int N, countdown;
    private Registry[] reg;
    private ComponentInterface[] stub;


    public StartupSynchro(int N) throws RemoteException, NotBoundException, MalformedURLException {

        this.N = N;
        this.countdown = N;
        reg = new Registry[N];
        stub = new ComponentInterface[N];
        System.err.println("Synchro server started. Countdown : "+countdown);

    }

    public void down() throws RemoteException, NotBoundException, MalformedURLException {
        countdown--;
        System.err.println("Countdown : "+countdown);
        System.err.flush();
        if (countdown <= 0 )
        {
            // create a separate thread to launch the startup. Another thread is needed to let the node exit the function call.
            Thread Launch = new Thread(new StartupSynchroLauncher());
            Launch.start();
        }
    }

    /*

    public void up() {
        countdownRound++;
        if (countdownRound >= N)
        {
            main_reassign();
        }
    }

    public void finish() {
        // registering neighbour
        Registry reg;
        ComponentInterface stub;
        for (int i = 0; i < N; i++)
        {
            try {
                reg = LocateRegistry.getRegistry(10000 + i);
                stub = (ComponentInterface) java.rmi.Naming.lookup("rmi://localhost:" + (10000 + i) + "/Receive");
                stub.finish();
            } catch (NotBoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }
    */
    public void main_startup() throws RemoteException, MalformedURLException, NotBoundException {
        for (int i = 0; i < N; i++)
        {
            reg[i] = LocateRegistry.getRegistry(10000 + i);
            stub[i] = (ComponentInterface) java.rmi.Naming.lookup("rmi://localhost:" + (10000 + i) + "/Receive");
        }
        for (int i = 0; i < N; i++)
            stub[i].connect();
        main_synchro();
    }


    public void main_synchro() throws RemoteException, MalformedURLException, NotBoundException {
        while (true) // do as many rounds as possible, till System.exit(0) kicks in
        {
            for (int i = 0; i < N; i++)
                stub[i].main_proc_nid(); // launch 1st step
            for (int i = 0; i < N; i++)
                stub[i].main_proc_nnid();
            for (int i = 0; i < N; i++)
                stub[i].main_proc_check();
            for (int i = 0; i < N; i++)
                stub[i].reassign_id();
        }

    }

}


