

package peterson;
        import java.net.MalformedURLException;
        import java.rmi.NotBoundException;
        import java.rmi.Remote;
        import java.rmi.RemoteException;



public interface StartupSynchroInterface extends Remote {
    /*
    public void up() throws  RemoteException, NotBoundException, MalformedURLException;
    public void finish() throws RemoteException;
    */

    public void down() throws RemoteException, NotBoundException, MalformedURLException;
    public void main_synchro() throws RemoteException, MalformedURLException, NotBoundException;
    public void main_startup() throws RemoteException, MalformedURLException, NotBoundException;

}
