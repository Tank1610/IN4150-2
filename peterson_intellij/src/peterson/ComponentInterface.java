package peterson;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;


public interface ComponentInterface extends Remote {
    public void receive_nid(int sender_id) throws RemoteException;
    public void receive_nnid(int sender_id) throws RemoteException, MalformedURLException, NotBoundException;

    public void main_proc_nid() throws  RemoteException, MalformedURLException, NotBoundException;
    public void main_proc_nnid() throws  RemoteException, MalformedURLException, NotBoundException;
    public void main_proc_check() throws  RemoteException, MalformedURLException, NotBoundException;

    public void connect() throws  RemoteException, MalformedURLException, NotBoundException;

    public void reassign_id() throws RemoteException, MalformedURLException, NotBoundException;
}